about me
>> age: 21
>> birthdate: September 1, 2000
>> zodiac sign: virgo
>> mbti: infp

Motivation in joining the bootcamp
>> I decided to join the bootcamp because I needed a confidence booster for my web development skills. Not only it will refresh what I already
know but it will also help me improve my skills and gain new knowledge. It will help me build my portfolio and give me a boost when finding a job as well.

Work experience
>> I used to work as a freelance virtual assistant/graphic designer on a part-time basis.